import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = 'https://test1.quadra-informatique.fr/api/auth/login';

  constructor(private http: HttpClient) { }

  login(login: string, password: string): any {
    const body = {'login': login,'password': password};
    return this.http.post<any>(this.url, body)
  };
  getCurrentUserToken(): string {
    return sessionStorage.getItem('token')!;
  }
  getCurrentUserName(): string {
    return sessionStorage.getItem('userName')!;
  }
  getCurrentUserId(): string {
    return sessionStorage.getItem('userId')!;
  }
}