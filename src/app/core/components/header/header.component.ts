import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/apis/login.service';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userName!: string;
  items!: MenuItem[];

  constructor(  private loginSrv: LoginService,
                private router: Router) { }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Déconnexion',
        icon: 'pi pi-sign-out',
        command: () => {
          this.disconnect();
        },
      }
    ];
    this.userName = this.loginSrv.getCurrentUserName();
  }

  disconnect(): void {
  this.userName = "";
  sessionStorage.removeItem('token');
  sessionStorage.removeItem('user');
  this.router.navigateByUrl('');
  }
}