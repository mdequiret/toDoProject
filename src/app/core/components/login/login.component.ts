import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/apis/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login!: string;
  password!: string;
  showError!: boolean;
  isLoggedIn: boolean = false;

  constructor(  private loginSrv: LoginService,
                private router: Router) { }

  async ngOnInit(): Promise<void> {}

  valid(): void {
    if (this.login && this.password ) {
      this.loginSrv.login(this.login, this.password).subscribe({
        next: (res: any) => {
          sessionStorage.setItem('token', res.token);
          sessionStorage.setItem('userName', res.user.name);
          sessionStorage.setItem('userId', res.user.id);
          this.showError = false;
        },
        error: () => {
          this.showError = true;
        },
        complete:() => this.router.navigateByUrl('to-do')
        });
    } else {
      this.showError = true;
    }
  }
}