export interface IToDo {
    todo_id: number | null;
    todo_user_id: number | null;
    todo_label: string;
    todo_date: Date | null;
    todo_is_done: boolean;
}
export class ToDo {
    todo_id!: number | null;
    todo_user_id!: number | null;
    todo_label!: string;
    todo_date!: Date | null;
    todo_is_done!: boolean;
  
    constructor(toDo?: IToDo) {
    this.todo_id = toDo ? toDo.todo_id : null;
    this.todo_user_id = toDo ? toDo.todo_user_id : null;
    this.todo_label = toDo ? toDo.todo_label : "";
    this.todo_date = toDo ? toDo.todo_date : null;
    this.todo_is_done = toDo ? toDo.todo_is_done : false;
    }   
  }