import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../core/services/apis/login.service';
@Injectable()
export class ToDoInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token: string = this.loginService.getCurrentUserToken();
    const userId: string = this.loginService.getCurrentUserId();
    const headers = {
                     "Authorization":`Bearer ${token}`,
                     "user-id":`${userId}`,
                     "Content-Type":"application/json"};
    return next.handle(request.clone({ setHeaders: headers }));
  }
}
