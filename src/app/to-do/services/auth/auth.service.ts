import { Injectable } from '@angular/core';
import { LoginService } from '../../../core/services/apis/login.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private loginSrv: LoginService) { }

  isLoggedIn(): boolean {
    if (this.loginSrv.getCurrentUserToken()) {
      return true;
    }
    return false;
  }
}