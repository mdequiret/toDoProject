import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ToDo } from '../../../core/models/to-do.model';
import { HttpClient, HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ToDoService {
    url = 'https://test1.quadra-informatique.fr/api/todo'

    constructor(private http: HttpClient) {}

    get(): Observable<ToDo[]> {
        const endpoint = this.url + '/list';
        return this.http.get<ToDo[]>(endpoint);
    }

    add(label: string, is_done: boolean): Observable<ToDo> {
        const body = {'todo_label':label,'todo_is_done':is_done}
            return this.http.post<ToDo>(this.url, body);
    };

    update(id: number, label: string, is_done: boolean): Observable<ToDo> {
        const body = {'todo_id':id,'todo_label':label,'todo_is_done':is_done}
            return this.http.post<ToDo>(this.url, body);
    };

    delete(id: number | null): Observable<boolean> {
        const endpoint = this.url + '/' + id;
        return this.http.delete<boolean>(endpoint);
    };

}