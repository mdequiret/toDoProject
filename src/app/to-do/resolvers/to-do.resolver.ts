import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { ToDo } from "src/app/core/models/to-do.model";
import { ToDoService } from "../services/api/to-do.service";

@Injectable()
export class ToDoResolver implements Resolve<ToDo[]> {

    constructor( private toDoService: ToDoService ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ToDo[]> {
        return this.toDoService.get()
    }
}