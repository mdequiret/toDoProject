import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { ToDoRoutingModule } from './to-do-routing.module';
import { ToDoListComponent } from './components/to-do-list/to-do-list.component';
import { ToDoComponent } from './components/to-do/to-do.component';
import { ToDoService } from './services/api/to-do.service';

import { ToDoResolver } from './resolvers/to-do.resolver';




@NgModule({
    declarations: [
        ToDoListComponent,
        ToDoComponent],
    imports: [
        CommonModule,
        ToDoRoutingModule,
        SharedModule,
    ],
    providers: [
        ToDoService,
        ToDoResolver,
    ],
    exports: []
})
export class ToDoModule {}