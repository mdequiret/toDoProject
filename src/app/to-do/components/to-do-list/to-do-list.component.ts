import { Component, OnInit } from '@angular/core';
import { ToDoService } from '../../services/api/to-do.service';
import { LoginService } from 'src/app/core/services/apis/login.service';
import { ToDo } from 'src/app/core/models/to-do.model';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {
  userId!: number;
  toDoList!: ToDo[];
  toDoToUpdate!: ToDo;
  displayDialog: boolean = false;

  constructor(private toDoSrv: ToDoService,
              private loginSrv: LoginService,
              private messageService: MessageService) {}

  ngOnInit(): void {
    this.userId = +this.loginSrv.getCurrentUserId();
    this.toDoSrv.get().subscribe((toDoList: ToDo[]) => {
      this.toDoList = toDoList.reverse();
    });
  }

  onCheck(toDo: ToDo): void {
    this.toDoSrv.update(<number> toDo.todo_id, toDo.todo_label, true).subscribe({
      next: (toDo: ToDo): void => {
        this.updateToDoList(toDo.todo_id, toDo);
      },
      error: (): void => {
        this.showError('checked');
      },
      complete: (): void => {
        this.showSuccess('checked');
      }
    })
  }

  onDelete(id: number | null): void {
    this.toDoSrv.delete(id).subscribe({
      next: (res: Boolean): void => {
        if (res) {
          this.toDoList = this.toDoList.filter(toDo => toDo.todo_id !== id);
        }
      },
      error: (): void => {
        this.showError('deleted');
      },
      complete: (): void => {
        this.showSuccess('deleted');
      }
    });
  }

  onUpdate(toDo: ToDo) {
    this.toDoToUpdate = Object.assign({}, toDo);
    this.onShowDialog();
  }

  onValidate(): void {
    if (!this.toDoToUpdate.todo_id) {
      this.toDoSrv.add(this.toDoToUpdate.todo_label, this.toDoToUpdate.todo_is_done).subscribe({
        next: (toDo: ToDo): void => {
          this.toDoList.unshift(toDo);
        },
        error: (): void => {
          this.showError('added');
        },
        complete: (): void => {
          this.showSuccess('added');
        }
      });
    } else {
      this.toDoSrv.update(this.toDoToUpdate.todo_id, this.toDoToUpdate.todo_label, this.toDoToUpdate.todo_is_done).subscribe({
        next: (toDo: ToDo): void => {
          this.updateToDoList(this.toDoToUpdate.todo_id, toDo);
        },
        error: (): void => {
          this.showError('updated');
        },
        complete: (): void => {
          this.showSuccess('updated');
        }
      });
    }
    this.onCloseDialog();
  }

  onAdd() {
    this.toDoToUpdate = new ToDo();
    this.onShowDialog();
  }

  onShowDialog(): void {
    this.displayDialog = true;
  }

  onCloseDialog(): void {
    this.displayDialog = false;
  }

  updateToDoList(id: number | null, newToDo: ToDo) {
    const index = this.toDoList.findIndex(toDo => toDo.todo_id === newToDo.todo_id);
    this.toDoList[index] = newToDo;
  }
  
  showSuccess(msg: string) {
    this.messageService.add({severity:'success', summary: 'Success', detail: `toDo ${msg} successfully !`});
  }

  showError(msg: string) {
      this.messageService.add({severity:'error', summary: 'Error', detail: `toDo wasn't ${msg}`});
  }
}