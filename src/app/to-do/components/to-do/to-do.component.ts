import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToDo } from 'src/app/core/models/to-do.model';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {

  // inputs
  @Input() toDo!: ToDo;
  // outputs
  @Output() onCheck: EventEmitter<ToDo> = new EventEmitter<ToDo>();
  @Output() onDelete: EventEmitter<number> = new EventEmitter<number>();
  @Output() onUpdate: EventEmitter<ToDo> = new EventEmitter<ToDo>();

  selectedCardIds: number[] = [];

  constructor() {}

  ngOnInit(): void {

  }

  onClickCheck(): void {
    this.onCheck.emit(this.toDo);
  }

  onClickDelete(): void {
    this.onDelete.emit(<number> this.toDo.todo_id);
  }

  onClickUpdate(): void {
    this.onUpdate.emit(this.toDo);
  }

}
