import { NgModule } from '@angular/core';

import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { MessageModule} from 'primeng/message';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { MenuModule } from 'primeng/menu';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';

@NgModule({
    exports: [
        CardModule,
        ButtonModule,
        DialogModule,
        InputTextModule,
        PasswordModule,
        MessageModule,
        ScrollPanelModule,
        MenuModule,
        ToastModule
    ],
    providers: [
        MessageService
    ]
})

export class PrimeNgModule {}