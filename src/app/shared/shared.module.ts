import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms';
import { PrimeNgModule } from './primeng.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
    ],
    exports: [
        PrimeNgModule,
        FormsModule
    ]
})
export class SharedModule {}