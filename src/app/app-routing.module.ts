// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
// import { ToDoListComponent } from './components/to-do-list/to-do-list.component';
// import { LoginComponent } from  './components/login/login.component'
// import { AuthGuard } from './services/auth/auth.guard'

// const routes: Routes = [
//   {path: 'toDoList', component: ToDoListComponent, canActivate: [AuthGuard]},
//   {path: '', component: LoginComponent},
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }

//--------------------------------------------------------------------------------------------------------------------------

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from  './core/components/login/login.component'

const routes: Routes = [
  {path: 'to-do', loadChildren: () => import('./to-do/to-do.module').then(m => m.ToDoModule)},
  {path: '**', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}