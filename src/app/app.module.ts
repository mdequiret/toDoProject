import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToDoInterceptor } from './interceptors/to-do.interceptor';

import { CoreModule } from './core/core.module'
import { SharedModule } from './shared/shared.module'



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    SharedModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, multi: true, useClass: ToDoInterceptor }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
